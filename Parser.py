import os
import json
import shutil
import datetime
import matplotlib.pyplot as plt
import numpy as np
from statistics import mean, stdev
from sympy import plot_implicit, Eq
from sympy.abc import x, y

devices = ["arduino_5"]
gateways = ["eui-58a0cbfffe800570", "eui-58a0cbfffe8003e5", "eui-58a0cbfffe800692"]
short_gateways = ["70", "E5", "92"]
#Coordonnées pour l'exp2 [[x70,xe5,x92],[y70,yE5,y92]]
gateways_coord = [[0, 5.32, 13.54], [0, 12.5, -1.77]]
device_coord = [[3.81], [3.59]]
#Coordonnées pour l'exp 1 [[x70,xe5,x92],[y70,yE5,y92]]
# device_coord = [[19.2], [23.72]]
# gateways_coord = [[0, 5.94, 43.29], [0, 33.73, 22.53]]
topic = "geoloc_indoor_rld/devices/arduino_5/up "
data_files = ["data/data_exp2.txt"]
data_to_extract = "time"
extracted_data = []

if __name__ == '__main__':

    if not os.path.exists("processed_data"):
        os.makedirs("processed_data")
        print("data directory created")

    for device in devices:
        if os.path.exists("processed_data/{}".format(device)):
            shutil.rmtree("processed_data/{}".format(device))
        os.makedirs(("processed_data/{}".format(device)))
        print("{} directory created".format(device))

    n = 0

    for data_file in data_files:
        with open(data_file) as file:
            data = file.read()
            packets = data.split(topic)[1:]
            for packet in packets:
                json_packet = json.loads(packet)
                device_id = json_packet['dev_id']
                packet_gateways = json_packet['metadata']['gateways']
                gateways_packet = []
                for done in packet_gateways:
                    gateways_packet.append(done['gtw_id'])
                check = all(gateway in gateways_packet for gateway in gateways)
                if check:
                    n += 1
                    values = []
                    for done in packet_gateways:
                        gateway = done['gtw_id']
                        if gateway in gateways:
                            with open("processed_data/{}/{}".format(device_id, gateway), "a") as file:
                                values.append(done[data_to_extract])
                                file.write(done[data_to_extract] + ";")
                    extracted_data.append(values)
                    with open("processed_data/{}/global".format(device_id), "a") as file:
                        for value in values:
                            file.write(value + ";")
                        file.write("\n")

    global_delta_t = []
    global_delta_t_bis = [[], [], []]
    for data in extracted_data:
        times = []
        for time in data:
            time = str.replace(time, "T", " ")
            time = str.replace(time, "Z", "")
            date = datetime.datetime.strptime(time[:-3], "%Y-%m-%d %H:%M:%S.%f")
            times.append(date)
        delta_t = {}
        k = 0
        for i in range(0, len(gateways)):
            for j in range(i + 1, len(gateways)):
                key = "delta(" + short_gateways[i] + "-" + short_gateways[j] + ")"
                value = abs(times[i] - times[j])
                delta_t[key] = value
                global_delta_t_bis[k].append((value.seconds * pow(10, 6) + value.microseconds))
                k += 1
        global_delta_t.append(delta_t)

    mean_delta_t = []
    stdev_delta_t = []
    distances = []
    labels = []

    for key, value in global_delta_t[0].items():
        labels.append(key)

    for val in global_delta_t_bis:
        mean_delta_t.append(mean(val))
        stdev_delta_t.append(stdev(val))

    stdev_delta_t = [i / 2 for i in stdev_delta_t]

    plt.bar(x=range(3), height=mean_delta_t, width=0.36, color="green")
    plt.errorbar(range(3), mean_delta_t, yerr=stdev_delta_t,
                 fmt='none', capsize=10, ecolor='red', elinewidth=2, capthick=2)
    plt.xticks(range(3), labels, rotation=45)
    plt.title("Delta t moyen entre les gateways")
    plt.ylabel("Temps en microsecondes (µs)")
    plt.show()
    plt.close()

    # Creating a numpy array
    x_gateways = np.array(gateways_coord[0])
    y_gateways = np.array(gateways_coord[1])
    x_devices = np.array(device_coord[0])
    y_devices = np.array(device_coord[1])
    # Plotting point using scatter method
    plt.scatter(x_gateways, y_gateways, c="red")
    plt.scatter(x_devices, y_devices, c="green", marker='*')
    plt.xlim(-0.2)
    plt.ylim(-2)
    plt.xlabel("Distance en mètres (m)")
    plt.ylabel("Distance en mètres (m)")
    plt.legend(["gateways", "device"])
    plt.title("Positions des gateways et du device dans l'espace")
    plt.show()

    print(mean_delta_t)
    distances = [i * 10 ** -6 * 3 * 10 ** 8 for i in mean_delta_t]
    print(distances)

    gateways_coord = [[0, 5.32, 13.54], [0, 12.5, -1.77]]
    # gateways_coord = [[0, 5.94, 43.29], [0, 33.73, 22.53]]

    #TDoA hyperbola calculation
    eq_1 = Eq((x ** 2 + y ** 2) ** (1 / 2) - ((5.32 - x) ** 2 + (12.5 - y) ** 2) ** 0.5, -1)
    plot_implicit(eq_1, (x, 0, 15), (y, -2, 15))

    eq_2 = Eq((x ** 2 + y ** 2) ** (1 / 2) - ((13.54 - x) ** 2 + (12.5 - y) ** 2) ** 0.5 , -1)
    plot_implicit(eq_2, (x, 0, 15), (y, -2, 15))

    eq_3 = Eq(((5.32 - x) ** 2 + (12.5 - y) ** 2) ** (1 / 2) - ((13.54 - x) ** 2 + (-1.77 - y) ** 2) ** 0.5, -1)
    plot_implicit(eq_3, (x, 0, 15), (y, -2, 15))


